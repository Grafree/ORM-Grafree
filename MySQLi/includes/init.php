<?php
use MySQLi\includes\Db;

/* Load Config file
 */
$config = parse_ini_file( 'config.ini' ); 

define( 'SITE_TITLE',     $config[ 'title' ] );
define( 'SITE_LANG',      $config[ 'lang' ] );
define( 'SITE_CHARSET',   $config[ 'charset' ] );
define( 'SITE_VERSION',   $config[ 'version' ] );
define( 'SITE_DEBUG',     $config[ 'debug' ] );


/* Sets the default timezone as recommended 
 */
date_default_timezone_set( $config['defaultTimezone'] );


/* Connects to DB
 */
include 'Db.php';
Db::connect( $config );


/*  
 * Unset config variable. Useless.
 */
unset( $config );


require( '../MySQLi/Orm.php' );