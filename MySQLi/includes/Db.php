<?php
namespace MySQLi\includes;

use Mysqli;

final class Db{
    
    private static $_connect;
    
    private function __construct(){}
    
    public static function connect( $config )
    {
        if( !isset( self::$_connect ) )
        {        
            self::$_connect = new mysqli( $config['dbhost'], $config['dbuser'], $config['dbpass'], $config['dbname'], $config['dbport'] );

            if( self::$_connect->connect_errno )
            {    
                $datas = self::$_connect->connect_error;
                
                var_dump( $datas );
                
                exit;
            }
        }
        
        return self::$_connect;
    }
    
    public static function db()
    {
        return self::$_connect;
    }
}