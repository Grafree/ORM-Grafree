<?php
namespace MySQLi\example;

use MySQLi\Orm;

class Articles {

    private $_maps = [ 
        'articles' =>
        [
            'IdArticle'         => [ 'type' => 'INT', 'autoincrement' => true, 'primary' => true ],
            'TitleArticle'      => [ 'type' => 'STR', 'mandatory' => true ],
            'ContentArticle'    => [ 'type' => 'TEXT', 'mandatory' => true ],
            'ImageArticle'      => [ 'type' => 'STR', 'file' => true ],
            'UrlArticle'        => [ 'type' => 'STR', 'default' => '' ],
            'DateArticle'       => [ 'type' => 'DATETIME', 'dateformat' => 'DD.MM.YYYY hh:ss:mm', 'default' => 'NOW' ],
            'IdCategory'        => [ 'type' => 'INT' ]
         ], 
        'categories' =>
        [
            'IdCategory'        => [ 'type' => 'INT', 'autoincrement' => true, 'primary' => true, 'dependencies' => ['articles'=>'IdCategory'] ],
            'NameCategory'      => [ 'type' => 'STR', 'mandatory' => true ]
        ],
        'relations' => [
            'articles' => [
                 'categories'   =>['articles'=>'IdCategory', 'categories'=>'IdCategory']
             ]
         ]
    ];

    
    public function select_articles()
    {
        $orm = new Orm( 'articles', $this->_maps['articles'], $this->_maps['relations'] );
        
        $ormQuery = $orm->select()
                        ->joins(['articles'=>['categories']])
                        ->execute();
        
        return $ormQuery;
    }
    
    

    public function select_article_first( $params = [] )
    {
        $orm = new Orm( 'articles', $this->_maps['articles'], $this->_maps['relations'] );

        $ormQuery = $orm->select()
                        ->joins(['articles'=>['categories']])
                        ->where($params)
                        ->first();
        
        return $ormQuery;
    }

    
    public function search_article( $params = [] )
    {
        $orm = new Orm( 'articles', $this->_maps['articles'], $this->_maps['relations'] );

        $ormQuery = $orm->select()
                        ->joins(['articles'=>['categories']])
                        ->wherelike($params)
                        ->first();
        
        return $ormQuery;
    }

    
    public function insert_article( $params = [] )
    {
        $orm = new Orm( 'articles', $this->_maps['articles'], $this->_maps['relations'] );
        
        $orm->prepareDatas($params);

        $ormQuery = $orm->insert();
        
        return $ormQuery;
    }

    
    public function update_article( $params = [], $update = [] )
    {
        $orm = new Orm( 'articles', $this->_maps['articles'], $this->_maps['relations'] );
        
        $orm->prepareDatas($params);

        $ormQuery = $orm->update( $update );
        
        return $ormQuery;
    }

    
    public function delete_article( $delete = [] )
    {
        $orm = new Orm( 'articles', $this->_maps['articles'], $this->_maps['relations'] );

        $ormQuery = $orm->delete($delete);
        
        return $ormQuery;
    }
}