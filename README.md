# ORM Grafree
Sets requests to the database. Uses the PDO PHP librairy.

From a mapping of a table of the database
sets select, insert, update and delete requests
*  It helps preparing and executing requests
*  It sanitizes datas that are selected, inserted, upated or sent back when errors occurs
*  It prepares datas for insert and update
*  It uploads files (needs, the upload and file class for that)
*  It manages errors

 All of these features are made with caution for security issues. 
 The excellent advises from phpdelusions has been very helpfull. 
 Aspecialy this PDO tutorial (https://phpdelusions.net/pdo)
 
| @param | Description |
| ------ | ------ |
|@param _string_ $dbTable  | Name of the table of the database|
|@param _array_ $mapping   | The mapping wich informs of the fields name in the <br>table and their specificities <br>(type, autoincrement, primary, mandatory, default, dateformat, file) <br>Types are : INT, STR, TEXT, DATE or DATETIME|
                        
### Example of use :

```php
$mapping = [
   'Id'           => [ 'type' => 'INT', 'autoincrement' => true, 'primary' => true, 'dependencies' => ['table'=>'IdField']  ],
   'Name'         => [ 'type' => 'STR', 'mandatory' => true ],
   'Infos'        => [ 'type' => 'TEXT', 'mandatory' => true ],
   'Date'         => [ 'type' => 'DATETIME', 'dateformat' => 'DD.MM.YYYY', 'default' => 'NOW' ],
   'Dateandtime'  => [ 'type' => 'DATETIME', 'default' => 'NOW' ],
   'File'         => [ 'type' => 'STR', 'file' => true ],
   'Active'       => [ 'type' => 'INT', 'mandatory' => true, 'default' => 0 ],  // Checkbox
]
```


| @param | Description |
| ------ | ------ |
|@param array $relations | Relations defined with the table for jointures and dependencies prendenting <br>from deleting datas related to another

### Example of use :
```php
$relations = 'relations' => [
      'dbTableName' => [
          'tableRelated'   =>['dbTableName'=>'IdFiledSecondary',  'tableRelated'=>'IdFieldPrimary'],
          'tableRelated2'  =>['dbTableName'=>'IdFiledSecondary2', 'tableRelated'=>'IdFieldPrimary']
      ]
  ]
```

### Examples of use :
```php
$orm = new Orm( 'dbTable', 'mapping' => ARRAY, 'relations' => ARRAY );
```

## SELECT
```php
$orm->select()
   ->join([ 'table1' => 'field1', 'table2' => 'field2' ])
   ->where([ 'IdCat' => INT ])
   ->wherelower([ 'Level' => '3' ])
   ->where([ 'IdLangue' => '1' ])
   ->wherenot([ 'Active' => '-1' ])
   ->wherelike( [ 'fields' => ['field1', 'filed2'], 'keywords' => ['word 1', 'word 2'] ] )
   ->group([ 'IdLang' => '2' ])
   ->order([ 'position'=>'ASC' ])
   ->limit([ 'num' => INT, 'nb' => INT ) // LIMIT nm, nb;
   ->execute();
```

## NUMBER OF ROWS
```php
$orm->count([ 'field1'=>'value', 'field2'=>'value' ]);      // return INT
```

## INSERT
```php
$orm->insert();                                             // return OBJECT
```

## UPDATE
```php
$orm->update([ 'id' => INT ]);                              // return OBJECT
```

## DELETE
```php
$orm->delete([ 'id' => INT ], 1 );
```